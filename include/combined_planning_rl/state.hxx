
#pragma once

#include <iostream>
#include <cstdlib>
#include <memory>
#include <set>
#include <vector>
#include <boost/functional/hash.hpp>


class State {
protected:
	//! Each position in the vector is a x,y or theta value corresponding to a model of the gazebo simulator
// 	std::vector<double> _values;
   	std::vector<int> _values;

	
	
	//Xr, Yr, Tr, Xo1, Yo1, Xo2, Yo2, B1, B2, H1, H2, J1, J2, J3, J4, H
	
public:
	typedef std::shared_ptr<const State> cptr;

	// The only allowed constructor receives all values making up the state
	State(std::vector<int> values) : _values(values) {}
	State() {}
	~State() {}
	
	void set_values(State& s) {_values = s._values;}
	//! Default copy constructors and assignment operators
	State(const State& state) = default;
	State( State&& state ) = default;
	State& operator=(const State &state) = default;
	State& operator=(State&& state) = default;
	
	int& operator[](unsigned idx) { return _values[idx]; }
	const int operator[](unsigned idx) const { return _values[idx]; }

	inline std::size_t size() const { return _values.size(); }

	//! Prints a representation of the state to the given stream.
	friend std::ostream& operator<<(std::ostream &os, State&  state) { return state.print(os); }
	
	std::ostream& print(std::ostream& os) {
	  //os << "State";
	 os << "";
	for (unsigned i = 0; i < _values.size(); ++i) {
		os << _values.at(i);
		if (i < _values.size() - 1) os << " ";
	}
	os << "";
	return os;
      }
	
	
	
	
	
	void set_value(unsigned idx, double value) {_values[idx] = value;}
	
protected:
	
};

