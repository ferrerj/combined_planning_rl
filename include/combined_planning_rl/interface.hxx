#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <boost/property_map/property_map.hpp>
#include "state.hxx"
#include "simulator.hxx"
#include "actions.hxx"
#include "sensor.hxx"
#include "dynamic_joint/attached_srv.h"
#include "gazebo_ros_link_attacher/Attach.h"
#include <cmath>
#include <geometry_msgs/Twist.h>


static const double m = 0.33333333333;



using namespace std;

class Interface {
  
  
private:
  
  int _action;
  vector<int> _actions;
  Simulator* sim;
  ros::NodeHandle _n;
  State* _current;
  ros::ServiceClient _attach_srv;
    ros::ServiceClient _dettach_srv;
    ros::Publisher _cmd_vel_pub;




public:
  
  
  Interface() {
    sim = new Simulator(_n);
    _attach_srv = _n.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/attach");
    _dettach_srv = _n.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/detach");
    _cmd_vel_pub = _n.advertise<geometry_msgs::Twist>("/cmd_vel", 10);

  }
  
  
  ~Interface() {}
  
  
  void set_to_init() {
   sim->set_to_init(); 
    
  }
  
  State get_current_state() {
      return sim->get_current_state();
  }
  
  void set_action(int action) {
      _action = action;
  }
  

  //Read action idx from file
  void read_action(std::string filename) {
    std::ifstream f(filename);
    std::string line;
    if (f.is_open()) {
      while ( getline (f,line) ) 
	_action = std::stoi(line);
      f.close();
  }
  //else std::cout << "Unable to open file"; 
    
 }
  
//   //Write adapted state to a file
  void output_state(State& s, std::string filename) {
   std:: ofstream myfile;
   myfile.open (filename);
   myfile << s;
   myfile.close();

 }
 
 void execute() {
  // cout << "EXECUTE: " << _action << endl;
   if(_action == 0)//Forward
     move();
   else if(_action == 1)//CW
     rotate(1);
   else if(_action == 2)//CCW
     rotate(2);
   else if(_action == 3)//Grasp o1
     grasp(1);
   else if(_action == 4)//Grasp o2
     grasp(2);
   else if(_action == 5)//Place o1
     place(1);
   else if(_action == 6)//Place 02
     place(2);
   else if(_action == 7)//Push B1
     push(1);
   else if(_action == 8)//Push B2
     push(2);
   else if(_action == 9)
     return;
   else if(_action == 10) {
     
     
     cout << "ACTION NUMBER 10 RECEIVED!!!!!!!!!!!!!!! OLEEEEEEEEEEE" << endl;
     std::string obj1 = "o1";
          std::string obj2 = "o2";
	  std::string wall = "brick_box_03x3x3";
	
     sim->set_to_init();
     system("rosservice call /gazebo/pause_physics");
     usleep(100000);
     ActionSimulator::move(5.5,3.5,0.0,wall,_n);
     system("rosservice call /gazebo/unpause_physics");

     usleep(100000);

   }
   
   
 }
 
 State move() {
  string model = "a_robot_big";
  vector<double> pos = sim->get_model_pos(model); 
    
  double x = pos.at(0);
  double y = pos.at(1);
  double yaw = pos.at(2);

  double angles[8] = {0, d_45, d_90, d_135, d_180, d_225, d_270, d_315  };

  double nx = cos(yaw)*m + x;
  double ny = sin(yaw)*m + y;
  double no = angles[Sensor::orientation(yaw)];
  ActionSimulator::move(nx, ny, no, model, _n);
  usleep(1000);
  
  
    if(sim->get_H() == 0)
        ActionSimulator::open_gripper(sim->joints(), _n);
    
  vector<double> new_pos = sim->get_model_pos(model); 
  usleep(1000);



   x = new_pos.at(0);
   y = new_pos.at(1);

  
  if(Sensor::on_button(x, y, 1))
    sim->set_B(1);
  else if(Sensor::on_button(x,y,2))
    sim->set_B(2);
  else sim->set_B(0);
  
  if(Sensor::inside_area(x, y, 1)) 
   sim->set_R(1);
  else if(Sensor::inside_area(x, y, 2)) 
   sim->set_R(2);
  else sim->set_R(0);
  
    string obj1 = "o1";
    string obj2 = "o2";

   vector<double> pos1 = sim->get_model_pos(obj1); 
   vector<double> pos2 = sim->get_model_pos(obj2); 
  
  double x1 = pos1.at(0); 
  double y1 = pos1.at(1);
  double x2 = pos2.at(0);
  double y2 = pos2.at(1);
  
  double x_from = -1.0;
  double x_to = 8.0;
  double y_from = -1.0;
  double y_to = 8.0;
  
  
 if(sim->get_H() != 1) 
  if(x1 < x_from || x1 > x_to || y1 < y_from || y1 > y_to)
     sim->set_model_state(0.0, 5.0, 0.0, obj1);
  
 if(sim->get_H() != 2) 
  if(x2 < x_from || x2 > x_to || y2 < y_from || y2 > y_to)
    sim->set_model_state(7.0, 3.0, 0.0, obj2);
  
 

  State s(get_current_state());
  return s;
   
 }
 
 
 void close() {
       ActionSimulator::close_gripper(sim->joints(), _n);
         usleep(1000);


 }
 
 void open() {
           ActionSimulator::open_gripper(sim->joints(), _n);
	     usleep(1000);


 }
 
 
  State rotate(int adir) {
    
  string model = "a_robot_big";
  vector<double> pos = sim->get_model_pos(model); 
  double x = pos.at(0);
  double y = pos.at(1);
  double yaw = pos.at(2);
  
  double angles[8] = {0, d_45, d_90, d_135, d_180, d_225, d_270, d_315  };

  int no = Sensor::orientation(yaw);
  double na = 0;
  if( adir == 1 ){ //CW
    na = angles[ (no+1)%8 ];
  }
  else{ //CCW
    if( no == 0) na = angles[ 7 ];
    else na = angles[ no - 1 ];
  }
  
  double nx = -1*cos(yaw)*0.1 + x;
  double ny = -1*sin(yaw)*0.1 + y;
  double new_yaw = angles[Sensor::orientation(yaw)];

  ActionSimulator::move(nx,ny,new_yaw,model,_n);
  usleep(1000);

  
  ActionSimulator::move(x,y,na,model,_n);
    usleep(1000);

  if(sim->get_H() == 0)
        ActionSimulator::open_gripper(sim->joints(), _n);



  State s(get_current_state());
  return s;
   
 }
 
 
 
 void grasp(int obj_idx) {
   string bug = "a_robot_big::bug";
   string palm = "a_robot_big::gripper_big_mov::palm";
   string obj = "";
   int factor = 4;
   if(obj_idx==1)
    obj="o1";
   else obj="o2";
   string lObj = obj+"::gl_";
   
   
   Point<double, 2> bug_pos= sim->get_link_pos(bug);
   Point<double, 2> palm_pos = sim->get_link_pos(palm);
   Point<double, 2> obj_pos = sim->get_link_pos(lObj);
   
   if(Sensor::correct_grasping(bug_pos, palm_pos, obj_pos)) {
     
     double angle = Geometry::angle_between(bug_pos, palm_pos, obj_pos);
     double dist = Geometry::distance(palm_pos, obj_pos) - 0.3;
     ActionSimulator::cmd_vel_action(1, 0.0, angle*factor, 1.0/factor, _cmd_vel_pub);
     usleep(10000);
     ActionSimulator::cmd_vel_action(0, dist*factor, 0.0, 1.0/factor, _cmd_vel_pub);
    usleep(10000);
     
     
     
    ActionSimulator::grasp(obj, _attach_srv );
     cout << "CORRECT GRASP of " << obj << "!!!!!!!!!!!" << endl;
    //ActionSimulator::grasp_fake(obj, _n);
    sim->set_H(obj_idx);
    ActionSimulator::close_gripper(sim->joints(), _n);
    usleep(1000);


   }
   else  {
    ActionSimulator::open_gripper(sim->joints(), _n);
    usleep(1000);
     sim->set_H(0);
   }
 }
 
 void place(int obj_idx) {
      string obj = "";
   if(obj_idx==1)
    obj="o1";
   else obj="o2";
   
  string model = "a_robot_big";
  vector<double> pos = sim->get_model_pos(model); 
  double x = pos.at(0);
  double y = pos.at(1);
  double yaw = pos.at(2);
   
      ActionSimulator::open_gripper(sim->joints(), _n);
        usleep(1000);

      ActionSimulator::place(obj,_dettach_srv);
      //ActionSimulator::place_fake(x,y,yaw,obj,_n);
        usleep(1000);

      sim->set_H(0);
      
  ActionSimulator::cmd_vel_action(3, 0.3 , 0.0 , 1.0, _cmd_vel_pub); 
  usleep(10000);

      
      
      

 }
 
 
 void move_wall(bool up) {
   system("rosservice call /gazebo/pause_physics");
   usleep(10000);
   string model = "brick_box_03x3x3";
     if(up)
      ActionSimulator::move(5.5, 6.5, 0.0, model, _n);
     else  ActionSimulator::move(5.5, 0.5, 0.0, model, _n);
    
    system("rosservice call /gazebo/unpause_physics");
    usleep(10000);

 }
 
 void push(int button) {
   
   cout << "PUSH BUTTON " << button << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
  string model = "a_robot_big";
  vector<double> pos = sim->get_model_pos(model); 
  double x = pos.at(0);
  double y = pos.at(1);
   
   
   //if(Sensor::on_button(button, x, y)) {
            cout << "Button " << button << " pressed" << endl;

     if(button==1) {
      move_wall(false);

     }
     else if(button==2) {
      move_wall(true);

     }
   //}

   
 }
 
 
  
  
  
  
  
  
  
};