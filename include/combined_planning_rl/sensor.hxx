#pragma once
#include "simulator.hxx"
#include "dynamic_joint/collision.h"

static const double d_45 = 0.785398;
static const double d_90 = 1.5708;
static const double d_135 = 2.35619;
static const double d_180 = 3.14159;
static const double d_315 = -0.785398;
static const double d_270 = -1.5708;
static const double d_225 = -2.35619;

using namespace std;


class Sensor {
  
  
private:
  
  
public:
  
  
  Sensor() {}
  ~Sensor() {}
  
  
  static int button_pressed(double x, double y) {
    
    double x1 = 1;
    double y1 = 1;
    double x2 = 1;
    double y2 = 1;
    
    if(x == x1 && y == y1)
      return 1;
    else if(x == x2 && y == y2)
      return 2; 
    else return 0;
    
  }
  
   //Check when robot collides with an static element
 static bool collide(ros::ServiceClient _collision_client) {
  
   bool robot_collision = false;
   dynamic_joint::collision collide;
   if(_collision_client.call(collide))
    robot_collision = collide.response.collision;
   
   //std::cout << "Collision: " << robot_collision << std::endl;
   return robot_collision;
   
 }
 
 static bool inside_area(double x, double y, int room) {
   /*
   double x_from1 = 3.3;
   double x_to1 = 7.5;
   double y_from1 = 5.3;
   double y_to1 = 7.6;
   double x_from2 = 3.3;
   double x_to2 = 7.5;
   double y_from2 = -0.6;
   double y_to2 =1.7;
   */
   
   double x_from1 = 3.3;
   double x_to1 = 6.0;
   double y_from1 = 5.3;
   double y_to1 = 7.6;
   double x_from2 = 3.3;
   double x_to2 = 6.0;
   double y_from2 = -0.6;
   double y_to2 =1.7;
   
   if(room == 1) {
     
    if(x > x_from1 && x < x_to1 && y > y_from1 && y < y_to1)
      return true;
    else return false;
   }
   else if(room == 2) {
         if(x > x_from2 && x < x_to2 && y > y_from2 && y < y_to2)
      return true;
    else return false;
        
   }
   
 }
 
 static bool on_button(double x, double y, int button ) {
   
   double x_from1 = 2.3;
   double x_to1 = 3.8;
   double y_from1 = 3.3;
   double y_to1 = 4.8;
   double x_from2 = 6.3;
   double x_to2 = 7.8;
   double y_from2 = 2.3;
   double y_to2 = 3.8;
   
   
   
   
/*   
   double x_from1 = 2.0;
   double x_to1 = 4.0;
   double y_from1 = 3.0;
   double y_to1 = 5.0;
   double x_from2 = 6.0;
   double x_to2 = 8.0;
   double y_from2 = 2.0;
   double y_to2 = 4.0;
   
   */
   if(button == 1) {
          
    if(x > x_from1 && x < x_to1 && y > y_from1 && y < y_to1)
      return true;
    else return false;

   }
   
   else if(button == 2) {
         if(x > x_from2 && x < x_to2 && y > y_from2 && y < y_to2)
      return true;
    else return false;
     
   }
   
 }
 
 static int orientation(double yaw) {

   double angles[8] = {0, d_45, d_90, d_135, d_180, d_225, d_270, d_315  };

   double dist_mini = 10;
   int best_angle = -1;
   for( int i = 0; i < 8; i++ ){
     double dist_ac = abs( angles[ i ] - yaw );
     if( dist_ac < dist_mini ){
       dist_mini = dist_ac;
       best_angle = i;
    }
   }
   return best_angle;
 }
 
 static bool correct_grasping(Point2D A, Point2D B, Point2D C) {
   
   
  // std::cout << "Angle: " << Geometry::angle_between(A, B, C) << std::endl;
  // std::cout << "Distance: " << Geometry::distance(B,C) << std::endl;
     
   
   if( (Geometry::angle_between(A, B, C) <= 3.1416) && Geometry::distance(B,C) < 1.5)//dist = 1.5; angle = 3.1416
     return true;
   else return false;
   
   
 }
 
  
  
  
};