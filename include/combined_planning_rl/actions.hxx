#pragma once

#include "state.hxx"
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <trajectory_msgs/JointTrajectory.h>
#include "dynamic_joint/attached.h"
#include "dynamic_joint/attach.h"
#include "gazebo_msgs/ContactsState.h"
#include <tf/transform_datatypes.h>
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/SetModelConfiguration.h>
#include <string>
#include "gazebo_ros_link_attacher/Attach.h"


using namespace std;

class State;

typedef std::string str;

class ActionSimulator {
  
  
public:
  
  
  ActionSimulator() {}
  ~ActionSimulator() {}
  
  static void cmd_vel_action(const int a, float v_trans, float v_rot, float time, const ros::Publisher& cmd_vel_pub) {

    double x, y, z = 0.0;
    
    if(a == 0) x = 1;
    else if(a == 1) z = 1;
    else if(a == 2) z = -1;
    else if(a == 3) x = -1;
    
    geometry_msgs::Twist cmd_vel_msg;
    cmd_vel_msg.linear.x = x*v_trans;
    cmd_vel_msg.linear.y = y*v_trans;
    cmd_vel_msg.angular.z = z*v_rot;
    cmd_vel_pub.publish(cmd_vel_msg);
    ros::WallDuration(time).sleep();
    stop_cmd_vel_action(cmd_vel_pub);
    usleep(1000);
  }
  
  static void stop_cmd_vel_action(const ros::Publisher& cmd_vel_pub) { 
    geometry_msgs::Twist cmd_vel_msg;
    cmd_vel_msg.linear.x = 0.0;
    cmd_vel_msg.linear.y = 0.0;
    cmd_vel_msg.angular.z = 0.0;
    cmd_vel_pub.publish(cmd_vel_msg);
    usleep(1000);
    
 }
 
 
 static bool move( double x,  double y,  double yaw, string model, ros::NodeHandle& n) {
   
	geometry_msgs::Pose to_pose;
	to_pose.position.x = x;
	to_pose.position.y = y;
	to_pose.position.z = 0.0;//TODO: By default, or maybe include it in the state?
	tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,yaw);
	set_quaternion(to_pose, q);
				
	gazebo_msgs::ModelState modelstate;
	modelstate.model_name = model;
	modelstate.reference_frame = (std::string) "world";
	modelstate.pose = to_pose;
		
	ros::ServiceClient client = n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
	gazebo_msgs::SetModelState setmodelstate;
	setmodelstate.request.model_state = modelstate;
	client.call(setmodelstate);
	usleep(10000);
	return setmodelstate.response.success;  

    
}

static void close_gripper(vector<string>& joints, ros:: NodeHandle& n) {
 set_model_configuration(true, n); 
}

static void open_gripper(vector<string>& joints, ros:: NodeHandle& n) {
  set_model_configuration(false, n); 
  
  
}


// Attach an object to the robot
 static void grasp(str& obj, ros::ServiceClient& attach_pub) {
   
   std::string link = obj+"::gl_";
   //std::string command = "rosservice call /attach \"{model1: 'a_robot_big', link1: 'a_robot_big::gripper_big_mov::palm', model2: '"+ obj +"', link2: '"+link+"', grasp: true}\"";
   //std::string command = "rosservice call /link_attacher_node/attach \"model_name_1: 'a_robot_big' link_name_1: 'a_robot_big::gripper_big_mov::palm' model_name_2: '"+obj+"' link_name_2: '"+link+"'\""; 
   //command = "rosservice call /link_attacher_node/attach \"model_name_1: 'a_robot_big' link_name_1: 'a_robot_big::gripper_big_mov::palm' model_name_2: 'o1' link_name_2: 'o1::gl_'\" ";
  // system(command.c_str());
   
   gazebo_ros_link_attacher::Attach attach;
   attach.request.model_name_1 = "a_robot_big";
//    attach.request.link_name_1 = "a_robot_big::gripper_big_mov::palm";
      attach.request.link_name_1 = "a_robot_big::bug";
   attach.request.model_name_2 = obj;
   attach.request.link_name_2 = link;
   attach_pub.call(attach);
     

   usleep(1000);

 }

 
 //Dettach an object from the robot
 static void place(str& obj, ros::ServiceClient& dettach_pub) {
      std::string link = obj+"::gl_";


   gazebo_ros_link_attacher::Attach attach;
   attach.request.model_name_1 = "a_robot_big";
//    attach.request.link_name_1 = "a_robot_big::gripper_big_mov::palm";
      attach.request.link_name_1 = "a_robot_big::bug";
   attach.request.model_name_2 = obj;
   attach.request.link_name_2 = link;
  dettach_pub.call(attach);
  
   usleep(10000);
   

 }
 
 
 static void grasp_fake(string obj, ros::NodeHandle& n) {
   move(500,500,0,obj,n);
   
 }
 
 static void place_fake(double x, double y, double theta, string& obj, ros::NodeHandle& n ) {
   
  double nx = cos(theta)*0.5 + x;
  double ny = sin(theta)*0.5 + y;
   move(nx,ny,0,obj,n);
   if(obj == "o1")
     move(7.5,7.5,0,obj,n);
   else if(obj == "o2")
      move(7.5,-0.5,0,obj,n);

 }
 
 
 //Set a model to a state configuration
static bool set_model_configuration(bool close, ros:: NodeHandle& n) {
   ros::ServiceClient client = n.serviceClient<gazebo_msgs::SetModelConfiguration>("/gazebo/set_model_configuration");
  gazebo_msgs::SetModelConfiguration setmodelconf;
  setmodelconf.request.model_name = "a_robot_big";
   
  if(close) {
    setmodelconf.request.joint_names.push_back("a_robot_big::gripper_big_mov::jright_finger");
    setmodelconf.request.joint_names.push_back("a_robot_big::gripper_big_mov::jleft_finger");
    setmodelconf.request.joint_names.push_back("a_robot_big::gripper_big_mov::jgr");
    setmodelconf.request.joint_names.push_back("a_robot_big::gripper_big_mov::jgl");
    setmodelconf.request.joint_positions.push_back(0.4); 
    setmodelconf.request.joint_positions.push_back(0.4); 
    setmodelconf.request.joint_positions.push_back(0.4); 
    setmodelconf.request.joint_positions.push_back(0.4); 
  }
  else {
    setmodelconf.request.joint_names.push_back("a_robot_big::gripper_big_mov::jright_finger");
    setmodelconf.request.joint_names.push_back("a_robot_big::gripper_big_mov::jleft_finger");
    setmodelconf.request.joint_names.push_back("a_robot_big::gripper_big_mov::jgr");
    setmodelconf.request.joint_names.push_back("a_robot_big::gripper_big_mov::jgl");
    setmodelconf.request.joint_positions.push_back(0.0); 
    setmodelconf.request.joint_positions.push_back(0.0); 
    setmodelconf.request.joint_positions.push_back(0.0); 
    setmodelconf.request.joint_positions.push_back(0.0); 
    
  }
    client.call(setmodelconf);
    
    
    
  
  usleep(10000);
  
  return true;
}


static void set_quaternion(geometry_msgs::Pose& pose, tf::Quaternion& q) {
	pose.orientation.x = q[0];
	pose.orientation.y = q[1];
	pose.orientation.z = q[2];
	pose.orientation.w = q[3];
}



static void approach_to_object(string& obj) {
  
  
  
}

 
  
  
protected:
  
  
  
};

