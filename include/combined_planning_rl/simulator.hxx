#pragma once

#include <ros/ros.h>
#include <gazebo_msgs/GetModelState.h>
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/GetWorldProperties.h>
#include <gazebo_msgs/GetModelProperties.h>
#include <gazebo_msgs/GetJointProperties.h>
#include <gazebo_msgs/SetModelConfiguration.h>
#include <gazebo_msgs/GetLinkState.h>
#include <tf/transform_datatypes.h>
#include "utils.hxx"
#include "state.hxx"
#include "sensor.hxx"

//Discretize robot orientation




static const int epsilon = 1;

//Discretize joints


using namespace std;

class Simulator {
  
private:
  
  ros::NodeHandle _n;
  std::vector<std::string> _models;
  std::vector<std::string> _joints;
  int _B, _R, _H;
  int _Dxo1, _Dyo1, _Dxo2, _Dyo2;
  int _Dir;
  int _Xr, _Yr;


  
public:
  
  
  Simulator(ros::NodeHandle n): _n(n) {
    
    _models.push_back("a_robot_big");
    _models.push_back("o1");
    _models.push_back("o2");
    
    _joints.push_back("a_robot_big::gripper_big_mov::jright_finger");
    _joints.push_back("a_robot_big::gripper_big_mov::jleft_finger");
    _joints.push_back("a_robot_big::gripper_big_mov::jgr");
    _joints.push_back("a_robot_big::gripper_big_mov::jgl");
    
    _B = _R = _H = 0;
    _Dxo1 = _Dyo1 = _Dxo2 = _Dyo2 = 0.0;
    _Dir = 0;
    _Xr = _Yr = 0;


    
  }
  
  ~Simulator() {}
  
  std::vector<std::string>& models() {return _models;}
  std::vector<std::string>& joints() {return _joints;}
  
  int get_B() {return _B;}
  int get_R() {return _R;}
  int get_H()  {return _H;}
  int get_Dxo1() {return _Dxo1; }
  int get_Dyo1() {return _Dyo1;}
  int get_Dxo2() {return _Dxo2;}
  int get_Dyo2() {return _Dyo2;}
  int get_dir() {return _Dir;}


  
  void set_B(int B) {_B = B;}
  void set_R(int R) {_R = R;}
  void set_H(int H) {_H = H;}
  void set_Dxo1(int Dxo1){_Dxo1 = Dxo1; }
  void set_Dyo1(int Dyo1) {_Dyo1 = Dyo1;}
  void set_Dxo2(int Dxo2) {_Dxo2 = Dxo2;}
  void set_Dyo2(int Dyo2) {_Dyo2 = Dyo2;}
  void set_Dir(int Dir) {_Dir = Dir;}
  
  


 //Get the current position of an element
 std::vector<double> get_model_pos(std::string& model) {
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state"); 
    gazebo_msgs::GetModelState getmodelstate;
    std::vector<double> values;

    getmodelstate.request.model_name= model;
    if(client.call(getmodelstate)) {
	values.push_back(getmodelstate.response.pose.position.x);
	values.push_back(getmodelstate.response.pose.position.y);
	tf::Quaternion q(getmodelstate.response.pose.orientation.x, getmodelstate.response.pose.orientation.y, 
		    getmodelstate.response.pose.orientation.z, getmodelstate.response.pose.orientation.w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw);  
	values.push_back(yaw);

      }
      
    usleep(1000);
    return values;
}

/*
Point3D get_model_pos(std::string& model) {
    double x, y = 0.0;
    double roll, pitch, yaw = 0.0;
    ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state"); 
    gazebo_msgs::GetModelState getmodelstate;
    std::vector<double> values;
    getmodelstate.request.model_name= model;
    if(client.call(getmodelstate)) {
	x = getmodelstate.response.pose.position.x;
	y = getmodelstate.response.pose.position.y;
	tf::Quaternion q(getmodelstate.response.pose.orientation.x, getmodelstate.response.pose.orientation.y, 
		    getmodelstate.response.pose.orientation.z, getmodelstate.response.pose.orientation.w);
	tf::Matrix3x3 m(q);
	m.getRPY(roll, pitch, yaw);  
      }

    usleep(1000);
    return Point3D(x,y,yaw);
}

*/

//Get the current position of a link
/*
 Point<double, 2>& get_link_pos(std::string link) {
  float x, y = 0.0;
   ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetLinkState>("/gazebo/get_link_state");
  gazebo_msgs::GetLinkState getlinkstate;
  std::vector<double> values;
  getlinkstate.request.link_name = link;
  getlinkstate.request.reference_frame = (std::string) "world";
  if(client.call(getlinkstate)) {
   values.push_back(getlinkstate.response.link_state.pose.position.x);
   values.push_back(getlinkstate.response.link_state.pose.position.y);
  }
  return values;
  
}
*/

 Point2D get_link_pos(std::string link) {
  float x, y = 0.0;
   ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetLinkState>("/gazebo/get_link_state");
  gazebo_msgs::GetLinkState getlinkstate;
  getlinkstate.request.link_name = link;
  getlinkstate.request.reference_frame = (std::string) "world";
  if(client.call(getlinkstate)) {
   x = getlinkstate.response.link_state.pose.position.x;
   y = getlinkstate.response.link_state.pose.position.y;
  }
  Point2D l(x,y);
  return l;
  
}



//Check is static or not
bool isStatic(std::string model) {
	 bool isstatic = false;
	 ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetModelProperties>("/gazebo/get_model_properties");
	 gazebo_msgs::GetModelProperties getmodelproperties; 
	 getmodelproperties.request.model_name= model;
	 if(client.call(getmodelproperties)) 
	    isstatic = getmodelproperties.response.is_static;
	return isstatic;
}

double  get_joint_properties(std::string& name) {
    ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetJointProperties>("/gazebo/get_joint_properties");
    gazebo_msgs::GetJointProperties getjointproperties;
    getjointproperties.request.joint_name=name;
    if(client.call(getjointproperties)) 
      return (double)getjointproperties.response.position.at(0);
}


std::vector<double>& get_current_configuration(int discr) {
     std::vector<double> tmp_values;
     for(unsigned i = 0; i < _joints.size(); ++i)
      tmp_values.push_back(get_joint_properties(_joints.at(i)));   
     
     return tmp_values;
}

  
void set_to_init() {
    
 
  std::string robot = "a_robot_big"; 
  std::string o1 = "o1";
  std::string o2 = "o2";
 std::string wall = "brick_box_03x3x3";
 
    set_model_state(0.0, 6.0, 0.0, robot);
    set_model_state(0.0, 5.0, 0.0, o1);
    set_model_state(7.0, 5.0, 0.0, o2);
    set_model_state(5.5, 3.5, 0.0, wall);
    
  std::vector<double> robot_pos = get_model_pos(robot);
  std::vector<double> o1_pos = get_model_pos(o1);
  std::vector<double> o2_pos = get_model_pos(o2);
  
  double xr = robot_pos.at(0);
  double yr = robot_pos.at(1);
  double yaw = robot_pos.at(2);
  double xo1 = o1_pos.at(0);
  double yo1 = o1_pos.at(1);
  double xo2 = o2_pos.at(0);
  double yo2 = o2_pos.at(1);
  _Xr = (int)(xr*discr) + epsilon;
  _Yr = (int)(yr*discr) + epsilon;
  _Dxo1 = Geometry::distance(xr, xo1);
  _Dyo1 = Geometry::distance(yr, yo1);
  _Dxo2 = Geometry::distance(xr, xo2);
  _Dyo2 = Geometry::distance(yr, yo2);
  _Dir = Sensor::orientation(yaw);
  _B = 0;
  _H = 0;
  _R = 0;

  }


//Dxo1, Dyo1, Dxo2, Dyo2, Dir, B1, B2, R1, R2, H
State get_current_state() {
  
  std::vector<int> state_values;
  std::string robot = "a_robot_big"; 
  std::string o1 = "o1";
  std::string o2 = "o2";
  
  std::vector<double> robot_pos = get_model_pos(robot);
  std::vector<double> o1_pos = get_model_pos(o1);
  std::vector<double> o2_pos = get_model_pos(o2);
  
  double xr = robot_pos.at(0);
  double yr = robot_pos.at(1);
  double yaw = robot_pos.at(2);
  double xo1 = o1_pos.at(0);
  double yo1 = o1_pos.at(1);
  double xo2 = o2_pos.at(0);
  double yo2 = o2_pos.at(1);
  
  
  if(_H == 0) {
  
  _Dxo1 = Geometry::distance(xr, xo1);
  _Dyo1 = Geometry::distance(yr, yo1);
  _Dxo2 = Geometry::distance(xr, xo2);
  _Dyo2 = Geometry::distance(yr, yo2);
  
  
  }
  
  else if(_H == 1) {
  _Dxo1 = Geometry::distance(xr, xr);
  _Dyo1 = Geometry::distance(yr, yr);
  _Dxo2 = Geometry::distance(xr, xo2);
  _Dyo2 = Geometry::distance(yr, yo2);
  }
  
  else if(_H == 2) {
  _Dxo1 = Geometry::distance(xr, xo1);
  _Dyo1 = Geometry::distance(yr, yo1);
  _Dxo2 = Geometry::distance(xr, xr);
  _Dyo2 = Geometry::distance(yr, yr);
    
  }
  
  _Xr = (int)(xr*discr) + epsilon;
  _Yr = (int)(yr*discr) + epsilon;
  
  
/*  
  if(_B == 0) {
      state_values.push_back(0);
      state_values.push_back(0);
  }
  
  else if(_B == 1) {
      state_values.push_back(1);
      state_values.push_back(0);
  }
    else if(_B == 2) {
      state_values.push_back(0);
      state_values.push_back(1);
  }
    if(_R == 0) {
      state_values.push_back(0);
      state_values.push_back(0);
  }
  
  else if(_R == 1) {
      state_values.push_back(1);
      state_values.push_back(0);
  }
    else if(_R == 2) {
      state_values.push_back(0);
      state_values.push_back(1);
  }
  
  if(_H == 0)
     state_values.push_back(0);
  else if(_H > 0)
     state_values.push_back(1);
  
  state_values.push_back(_Xr);
  state_values.push_back(_Yr);

  
  _Dir = Sensor::orientation(yaw);
  state_values.push_back(_Dir);

  state_values.push_back(_Dxo1);
  state_values.push_back(_Dyo1);
  state_values.push_back(_Dxo2);
  state_values.push_back(_Dyo2);
  


  state_values.push_back(_B);
  state_values.push_back(_R);
  state_values.push_back(_H);
  
  */
  
  state_values.push_back(_Xr);
  state_values.push_back(_Yr);
    _Dir = Sensor::orientation(yaw);
  state_values.push_back(_Dir);
  
  state_values.push_back(_Dxo1);
  state_values.push_back(_Dyo1);
  state_values.push_back(_Dxo2);
  state_values.push_back(_Dyo2);

  
  state_values.push_back(_B);
  state_values.push_back(_R);
  state_values.push_back(_H);

  
  
  return State(state_values);
	

}

bool set_model_state(double x, double y, double theta, std::string model)  {
		geometry_msgs::Pose to_pose;
		to_pose.position.x = x;
		to_pose.position.y = y;
		to_pose.position.z = 0.0;//TODO: By default, or maybe include it in the state?
		tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,theta);
		set_quaternion(to_pose, q);
		
						
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = model;
		modelstate.reference_frame = (std::string) "world";
		modelstate.pose = to_pose;
		
		ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		client.call(setmodelstate);
		//ros::WallDuration(0.1).sleep();
		usleep(10000);
		return setmodelstate.response.success;
}


bool set_model_configuration(double to) {
  ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelConfiguration>("/gazebo/set_model_configuration");
  gazebo_msgs::SetModelConfiguration setmodelconf;
  setmodelconf.request.model_name = "tiago_steel";
  
  double from = 0.0;
  
  while (from < to){
  
  for(unsigned i = 0; i < _joints.size(); ++i) {
    setmodelconf.request.joint_names.push_back(_joints.at(i));
   setmodelconf.request.joint_positions.push_back(from); 
  }
    client.call(setmodelconf);
      from = from+0.05;
    
  }
  
  return true;
  
  
}



void set_quaternion(geometry_msgs::Pose& pose, tf::Quaternion& q) const {
	pose.orientation.x = q[0];
	pose.orientation.y = q[1];
	pose.orientation.z = q[2];
	pose.orientation.w = q[3];
}




  
  
  
  
};